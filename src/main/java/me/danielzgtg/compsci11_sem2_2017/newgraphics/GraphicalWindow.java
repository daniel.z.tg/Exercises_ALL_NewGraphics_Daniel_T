package me.danielzgtg.compsci11_sem2_2017.newgraphics;

import me.danielzgtg.compsci11_sem2_2017.common.datastructure.Pair;
import me.danielzgtg.compsci11_sem2_2017.common.platform.MainThreadUtils;
import me.danielzgtg.compsci11_sem2_2017.common.program.Program;
import me.danielzgtg.compsci11_sem2_2017.common.reflect.Event;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;
import org.lwjgl.opengl.GL;

import static org.lwjgl.glfw.GLFW.GLFW_CONTEXT_VERSION_MAJOR;
import static org.lwjgl.glfw.GLFW.GLFW_CONTEXT_VERSION_MINOR;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_CORE_PROFILE;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_FORWARD_COMPAT;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_PROFILE;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_SAMPLES;
import static org.lwjgl.glfw.GLFW.GLFW_STENCIL_BITS;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwSetCursorPosCallback;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetMouseButtonCallback;
import static org.lwjgl.glfw.GLFW.glfwSetScrollCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowTitle;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.system.MemoryUtil.NULL;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class GraphicalWindow {

	private final GLFWKeyCallback glfwKeyCallback;
	private final GLFWCursorPosCallback glfwCursorPosCallback;
	private final GLFWMouseButtonCallback glfwMouseButtonCallback;
	private final GLFWScrollCallback glfwScrollCallback;

	public final Event<Pair<Integer, Integer>> keyPressEvent = new Event<>();
	public final Event<Pair<Integer, Integer>> keyReleaseEvent = new Event<>();
	public final Event<Pair<Double, Double>> mouseMoveEvent = new Event<>();
	public final Event<Integer> mousePressEvent = new Event<>();
	public final Event<Integer> mouseReleaseEvent = new Event<>();
	public final Event<Pair<Double, Double>> scrollEvent = new Event<>();

	public final long id;
	private volatile boolean active = true;

	private GraphicalWindow(final long id) {
		this.id = id;

		this.glfwKeyCallback = GLFWKeyCallback.create(this::onKey);
		this.glfwCursorPosCallback = GLFWCursorPosCallback.create(this::onMouseMove);
		this.glfwMouseButtonCallback = GLFWMouseButtonCallback.create(this::onMouseButton);
		this.glfwScrollCallback = GLFWScrollCallback.create(this::onScroll);

		glfwSetKeyCallback(this.id, this.glfwKeyCallback);
		glfwSetCursorPosCallback(this.id, this.glfwCursorPosCallback);
		glfwSetMouseButtonCallback(this.id, this.glfwMouseButtonCallback);
		glfwSetScrollCallback(this.id, this.glfwScrollCallback);
	}

	public long getId() {
		return this.id;
	}

	public boolean isActive() {
		return this.active;
	}

	public synchronized void cleanup() {
		if (this.active) {
			MainThreadUtils.run(() -> {
				this.glfwKeyCallback.free();
				this.glfwCursorPosCallback.free();
				this.glfwMouseButtonCallback.free();
				this.glfwScrollCallback.free();

				glfwDestroyWindow(this.id);
			}, true);

			this.active = false;
		}
	}

	public final boolean shouldClose() {
		return !this.active || glfwWindowShouldClose(this.id);
	}

	public final boolean updateShouldStop(final Program program) {
		final boolean result = this.shouldClose();

		if (result) {
			program.stop();
			return true;
		}

		return !program.running();
	}

	public final void setTitle(final String newTitle) {
		if (!this.active) {
			throw new GraphicalException();
		}

		glfwSetWindowTitle(this.id, newTitle);
	}

	public final void flipBuffers() {
		if (!this.active) {
			throw new GraphicalException();
		}

		glfwSwapBuffers(this.id);
	}

	public final void bind() {
		if (!this.active) {
			throw new GraphicalException();
		}

		glfwMakeContextCurrent(this.id);
	}

	public static final void unbind() {
		glfwMakeContextCurrent(NULL);
	}

	public static final GraphicalWindow create(final int initialWidth, final int initialHeight,
			final String initialTitle, final boolean antiAliased) {
		GraphicalSystem.ensure();

		final GraphicalWindow[] resultContainer = new GraphicalWindow[1];
		MainThreadUtils.run(() -> {
			if (antiAliased) {
				glfwWindowHint(GLFW_STENCIL_BITS, 4);
				glfwWindowHint(GLFW_SAMPLES, 4);
			}

			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
			glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
			glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

			final long id = glfwCreateWindow(initialWidth, initialHeight, initialTitle, NULL, NULL);
			if (id != NULL){
				resultContainer[0] = new GraphicalWindow(id);
			}
		}, true);

		final GraphicalWindow result = resultContainer[0];
		if (result == null) {
			throw new GraphicalException("Window creation failed!");
		}

		result.bind();
		GL.createCapabilities();

		glfwSwapInterval(1);

		return result;
	}

	private void onKey(final long window, final int key, final int scancode, final int action, final int mods) {
		switch (action) {
			case GLFW_PRESS:
				this.keyPressEvent.fire(new Pair<>(key, mods));
				break;
			case GLFW_RELEASE:
				this.keyReleaseEvent.fire(new Pair<>(key, mods));
				break;
		}
	}

	private void onMouseMove(final long window, final double xpos, final double ypos) {
		this.mouseMoveEvent.fire(new Pair<>(xpos, ypos));
	}

	private void onMouseButton(final long window, final int button, final int action, final int mods) {
		switch (action) {
			case GLFW_PRESS:
				this.mousePressEvent.fire(button);
				break;
			case GLFW_RELEASE:
				this.mouseReleaseEvent.fire(button);
				break;
		}
	}

	private void onScroll(final long window, final double xoffset, final double yoffset) {
		this.scrollEvent.fire(new Pair<>(xoffset, yoffset));
	}
}
