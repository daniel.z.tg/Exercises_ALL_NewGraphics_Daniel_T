package me.danielzgtg.compsci11_sem2_2017.newgraphics.primitive;

@SuppressWarnings("WeakerAccess")
public abstract class Primitive {

	public static final int INDEX_ELEMENTS = 3;
	public static final int VERTEX_ELEMENTS = 9;

	public final Model parent;
	public final int triangles;

	/*packaged*/ boolean dirty = true;
	/*packaged*/ boolean verticesDirty = true;
	/*packaged*/ boolean indicesDirty = true;
	/*packaged*/ final float[] vertices;
	/*packaged*/ final short[] indices;

	/*packaged*/ Primitive(final float[] vertices, final short[] indices, final Model parent) {
		if (vertices.length == 0 | indices.length == 0 || parent == null
				|| vertices.length % VERTEX_ELEMENTS != 0 || indices.length % INDEX_ELEMENTS != 0) {
			throw new IllegalArgumentException();
		}

		this.vertices = vertices;
		this.indices = indices;
		this.parent = parent;
		this.triangles = indices.length / 3;
	}

	public abstract void show();

	public abstract void hide();

	public final void setVertex(final float[] vertexData, final int index) {
		if (vertexData == null || vertexData.length != VERTEX_ELEMENTS || index < 0 || index > this.vertices.length) {
			throw new IllegalArgumentException();
		}

		System.arraycopy(vertexData, 0, this.vertices, index * VERTEX_ELEMENTS, VERTEX_ELEMENTS);

		this.dirty = true;
		this.verticesDirty = true;
	}

	public final void copyVertex(final float[] vertexBuffer, final int index) {
		if (vertexBuffer == null || vertexBuffer.length != VERTEX_ELEMENTS || index < 0 || index > this.vertices.length) {
			throw new IllegalArgumentException();
		}

		System.arraycopy(this.vertices, index * VERTEX_ELEMENTS, vertexBuffer, 0, VERTEX_ELEMENTS);
	}

	public final Model getParent() {
		return this.parent;
	}

	public final int getTriangles() {
		return this.triangles;
	}
}
