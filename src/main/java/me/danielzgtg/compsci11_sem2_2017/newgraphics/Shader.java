package me.danielzgtg.compsci11_sem2_2017.newgraphics;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_INFO_LOG_LENGTH;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glDeleteProgram;
import static org.lwjgl.opengl.GL20.glDeleteShader;
import static org.lwjgl.opengl.GL20.glDetachShader;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetShaderi;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL20.glValidateProgram;

@SuppressWarnings("WeakerAccess")
public final class Shader {

	private final String vsh;
	private final String fsh;
	private volatile int vshID;
	private volatile int fshID;
	private volatile int programID;
	private final Map<String, Integer> uniformPositions = new HashMap<>();
	private final List<String> uniforms;

	private Shader(final String vsh, final String fsh, final List<String> uniforms) {
		this.vsh = vsh;
		this.fsh = fsh;
		this.uniforms = uniforms;
	}

	public static final Shader of(final String vsh, final String fsh, final List<String> uniforms) {
		if (vsh == null || fsh == null || uniforms == null) {
			throw new IllegalArgumentException();
		}

		final List<String> checkUniforms = Collections.unmodifiableList(new LinkedList<>(uniforms));

		for (final String s : checkUniforms) {
			if (s == null) {
				throw new IllegalArgumentException();
			}
		}

		return new Shader(vsh, fsh, checkUniforms);
	}

	public final void reupload() {
		this.cleanup();

		this.programID = glCreateProgram();

		if (this.programID == 0) {
			throw new GraphicalException();
		}

		GLUtils.assertNoError();

		this.vshID = createShaderPart(this.vsh, this.programID, GL_VERTEX_SHADER);
		this.fshID = createShaderPart(this.fsh, this.programID, GL_FRAGMENT_SHADER);

		glLinkProgram(this.programID);
		glValidateProgram(this.programID);

		GLUtils.assertNoLinkError(this.programID, this.vshID, this.fshID);

		this.bind();

		for (final String uniform : this.uniforms) {
			final int pos = glGetUniformLocation(this.programID, uniform);

			if (pos < 0) {
				throw new GraphicalException("Unknown uniform: " + uniform);
			}

			this.uniformPositions.put(uniform, pos);
		}
	}

	public int getUniform(final String s) {
		return this.uniformPositions.get(s);
	}

	public void cleanup() {
		Shader.unbind();

		if (this.programID != 0) {
			glDetachShader(this.programID, this.vshID);
			glDetachShader(this.programID, this.fshID);
			glDeleteShader(this.vshID);
			glDeleteShader(this.fshID);
			glDeleteProgram(this.programID);

			this.uniformPositions.clear();
			this.programID = 0;
		}
	}

	@Override
	public void finalize() {
		if (this.programID != 0) {
			System.err.println("[Shader] GPU TEXTURE SHADER LEAK!" + this.programID);
		}
	}

	public void bind() {
		if (this.programID == 0) {
			throw new GraphicalException();
		}

		glUseProgram(this.programID);
	}

	public static final void unbind() {
		glUseProgram(0);
	}

	private static int createShaderPart(final String source, final int programID, final int shaderType) {
		if (source == null || shaderType == 0) {
			throw new IllegalArgumentException();
		}

		if (programID == 0) {
			throw new GraphicalException();
		}

		final int result = glCreateShader(shaderType);
		if (result < 0) {
			throw new GraphicalException();
		}

		glShaderSource(result, source);
		glCompileShader(result);

		if (glGetShaderi(result, GL_COMPILE_STATUS) == GL_FALSE) {
			glDeleteShader(result);
			throw new GraphicalException("Shader error\n" +
					glGetShaderInfoLog(result, glGetShaderi(result, GL_INFO_LOG_LENGTH)));
		}

		glAttachShader(programID, result);

		GLUtils.assertNoShaderError(result);
		return result;
	}
}
