package me.danielzgtg.compsci11_sem2_2017.newgraphics;

import java.nio.FloatBuffer;

import me.danielzgtg.compsci11_sem2_2017.common.MathUtils;

import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;

public final class RelativeSubview {

	private boolean dirty = true;
	private float posX, posY, posZ, yaw, pitch;

	public final float getPosX() {
		return this.posX;
	}

	public final float getPosY() {
		return this.posY;
	}

	public final float getPosZ() {
		return this.posZ;
	}

	public final float getYaw() {
		return this.yaw;
	}

	public final float getPitch() {
		return this.pitch;
	}

	public void setPos(final float posX, final float posY, final float posZ) {
		this.dirty = true;

		this.posX = posX;
		this.posY = posY;
		this.posZ = posZ;
	}

	public void setPosX(final float posX) {
		this.dirty = true;

		this.posX = posX;
	}

	public void setPosY(final float posY) {
		this.dirty = true;

		this.posY = posY;
	}

	public void setPosZ(final float posZ) {
		this.dirty = true;

		this.posZ = posZ;
	}

	public void setRot(final float yaw, final float pitch) {
		this.dirty = true;

		this.yaw = MathUtils.normalizeAngleDeg(yaw);
		this.pitch = MathUtils.normalizeAngleDeg(pitch);
	}

	public void setYaw(final float yaw) {
		this.dirty = true;

		this.yaw = MathUtils.normalizeAngleDeg(yaw);
	}

	public void setPitch(final float pitch) {
		this.dirty = true;

		this.pitch = MathUtils.normalizeAngleDeg(pitch);
	}

	public void setView(final float posX, final float posY, final float posZ, final float yaw, final float pitch) {
		this.dirty = true;

		this.posX = posX;
		this.posY = posY;
		this.posZ = posZ;

		this.yaw = MathUtils.normalizeAngleDeg(yaw);
		this.pitch = MathUtils.normalizeAngleDeg(pitch);
	}

	public void updateMatrix(final float[] scratchMatrix, final FloatBuffer scratchBuffer, final int uniformLocation,
			final boolean isCamera) {
		if (this.dirty) {
			float pCos = (float) Math.cos(Math.toRadians(this.pitch));
			float pSin = (float) Math.sin(Math.toRadians(this.pitch));

			float wCos = (float) Math.cos(Math.toRadians(this.yaw));
			float wSin = (float) Math.sin(Math.toRadians(this.yaw));

			final float rotA = pSin * wSin;
			final float rotB = pCos * -wSin;
			final float rotC = -pSin * wCos;
			final float rotD = pCos * wCos;

			final int factor = isCamera ? -1 : 1;
			final float transX = this.posX * factor;
			final float transY = this.posY * factor;
			final float transZ = this.posZ * factor;

			scratchMatrix[ 0] = wCos;
			scratchMatrix[ 1] = rotA;
			scratchMatrix[ 2] = rotB;
			scratchMatrix[ 3] = 0;

			scratchMatrix[ 4] = 0;
			scratchMatrix[ 5] = pCos;
			scratchMatrix[ 6] = pSin;
			scratchMatrix[ 7] = 0;

			scratchMatrix[ 8] = wSin;
			scratchMatrix[ 9] = rotC;
			scratchMatrix[10] = rotD;
			scratchMatrix[11] = 0;

			scratchMatrix[12] = wCos * transX + wSin * transZ;
			scratchMatrix[13] = rotA * transX + pCos * transY + rotC * transZ;
			scratchMatrix[14] = rotB * transX + pSin * transY + rotD * transZ;
			scratchMatrix[15] = 1;

			scratchBuffer.put(scratchMatrix).flip();
			glUniformMatrix4fv(uniformLocation, false, scratchBuffer);

			this.dirty = false;
		}
	}
}
