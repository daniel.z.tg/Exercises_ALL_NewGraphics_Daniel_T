package me.danielzgtg.compsci11_sem2_2017.newgraphics;

import java.nio.FloatBuffer;

import me.danielzgtg.compsci11_sem2_2017.newgraphics.primitive.Primitive;

@SuppressWarnings("WeakerAccess")
public final class GraphicalBuffers {

	public /*mut*/ final float[] scratchMatrix = new float[16];
	public /*mut*/ final float[] scratchVertex = new float[Primitive.VERTEX_ELEMENTS];
	public /*mut*/ final FloatBuffer scratchBuffer = GLUtils.createMatrixBuffer();
}
