package me.danielzgtg.compsci11_sem2_2017.newgraphics;

import java.nio.FloatBuffer;

import me.danielzgtg.compsci11_sem2_2017.newgraphics.primitive.Model;

import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;

@SuppressWarnings("WeakerAccess")
public final class Camera {

	private CameraShaderData camData;

	public final GraphicalBuffers buffers;
	public final RelativeSubview cameraView = new RelativeSubview();

	private boolean projDirty = true;
	private float nearPlane, farPlane, fov, aspectRatio;

	public Camera(final CameraShaderData camData, final GraphicalBuffers buffers,
			final float nearPlane, final float farPlane, final float fov, final float aspectRatio) {
		this.setCamData(camData);

		this.buffers = buffers;

		this.nearPlane = nearPlane;
		this.farPlane = farPlane;
		this.fov = fov;
		this.aspectRatio = aspectRatio;
	}

	public final void setCamData(final CameraShaderData camData) {
		if (camData == null) {
			throw new IllegalArgumentException();
		}

		this.camData = camData;
	}

	public RelativeSubview getCameraView() {
		return this.cameraView;
	}

	public float getPosX() {
		return this.cameraView.getPosX();
	}

	public float getPosY() {
		return this.cameraView.getPosY();
	}

	public float getPosZ() {
		return this.cameraView.getPosZ();
	}

	public float getYaw() {
		return this.cameraView.getYaw();
	}

	public float getPitch() {
		return this.cameraView.getPitch();
	}

	public void setPos(final float posX, final float posY, final float posZ) {
		this.cameraView.setPos(posX, posY, posZ);
	}

	public void setPosX(final float posX) {
		this.cameraView.setPosX(posX);
	}

	public void setPosY(final float posY) {
		this.cameraView.setPosY(posY);
	}

	public void setPosZ(final float posZ) {
		this.cameraView.setPosZ(posZ);
	}

	public void setRot(final float yaw, final float pitch) {
		this.cameraView.setRot(yaw, pitch);
	}

	public void setYaw(final float yaw) {
		this.cameraView.setYaw(yaw);
	}

	public void setPitch(final float pitch) {
		this.cameraView.setPitch(pitch);
	}

	public void setView(final float posX, final float posY, final float posZ, final float yaw, final float pitch) {
		this.cameraView.setView(posX, posY, posZ, yaw, pitch);
	}

	public float getNearPlane() {
		return this.nearPlane;
	}

	public float getFarPlane() {
		return this.farPlane;
	}

	public float getFov() {
		return this.fov;
	}

	public float getAspectRatio() {
		return this.aspectRatio;
	}

	public void setNearPlane(final float nearPlane) {
		this.projDirty = true;

		this.nearPlane = nearPlane;
	}

	public void setFarPlane(final float farPlane) {
		this.projDirty = true;

		this.farPlane = farPlane;
	}

	public void setFov(final float fov) {
		this.projDirty = true;

		this.fov = fov;
	}

	public void setAspectRatio(final float aspectRatio) {
		this.projDirty = true;

		this.aspectRatio = aspectRatio;
	}

	public void flipBuffers() {
		final float[] scratchMatrix = this.buffers.scratchMatrix;
		final FloatBuffer scratchBuffer = this.buffers.scratchBuffer;

		if (this.projDirty) {
			final float yScale = 1.0F / (float) Math.tan(Math.toRadians(this.fov / 2));
			final float xScale = yScale / this.aspectRatio;
			final float nf = this.nearPlane - this.farPlane;


			scratchMatrix[ 0] = xScale;
			scratchMatrix[ 1] = 0;
			scratchMatrix[ 2] = 0;
			scratchMatrix[ 3] = 0;

			scratchMatrix[ 4] = 0;
			scratchMatrix[ 5] = yScale;
			scratchMatrix[ 6] = 0;
			scratchMatrix[ 7] = 0;

			scratchMatrix[ 8] = 0;
			scratchMatrix[ 9] = 0;
			scratchMatrix[10] = ((this.farPlane + this.nearPlane) / nf);
			scratchMatrix[11] = -1;

			scratchMatrix[12] = 0;
			scratchMatrix[13] = 0;
			scratchMatrix[14] = (2 * this.nearPlane * this.farPlane) / nf;
			scratchMatrix[15] = 0;

			scratchBuffer.put(scratchMatrix).flip();
			glUniformMatrix4fv(this.camData.projMatrixLocation, false, scratchBuffer);

			this.projDirty = false;
		}

		this.cameraView.updateMatrix(scratchMatrix, scratchBuffer, this.camData.viewMatrixLocation, true);
	}

	public void lookThrough(final Model model) {
		if (model == null) {
			throw new IllegalArgumentException();
		}

		model.modelView.updateMatrix(this.buffers.scratchMatrix, this.buffers.scratchBuffer,
				this.camData.modelMatrixLocation, false);
	}

}
