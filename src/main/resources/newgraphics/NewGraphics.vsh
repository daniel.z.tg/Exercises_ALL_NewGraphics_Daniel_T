#version 330 core

layout(location = 0) in vec3 position;
layout(location = 3) in vec4 color;
layout(location = 8) in vec2 texCoord;

uniform mat4 projM;
uniform mat4 viewM;
uniform mat4 modelM;

out vec4 vColor;
out vec2 vTexCoord;

void main()
{
    gl_Position = projM * viewM * modelM * vec4(position, 1.0);

    vColor = color;
    //vColor = vec4(projM[0][0], projM[0][1], projM[0][2], 1.0);
    vTexCoord = texCoord;
}
