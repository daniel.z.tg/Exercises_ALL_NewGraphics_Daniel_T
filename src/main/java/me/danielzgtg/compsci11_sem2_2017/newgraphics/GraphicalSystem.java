package me.danielzgtg.compsci11_sem2_2017.newgraphics;

import me.danielzgtg.compsci11_sem2_2017.common.Log;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.Pair;
import me.danielzgtg.compsci11_sem2_2017.common.platform.MainThreadUtils;
import me.danielzgtg.compsci11_sem2_2017.common.reflect.Event;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;

import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWaitEvents;

@SuppressWarnings("WeakerAccess")
public class GraphicalSystem {
	private static volatile int users = 0;
	private static Thread pollThread;

	private static volatile GLFWErrorCallback glfwErrorCallback;

	public static final Event<Pair<Integer, Long>> errorEvent = new Event<>();

	public static synchronized final void require() {
		if (users++ == 0) {
			try {
				MainThreadUtils.run(() -> {
					if (!glfwInit()) {
						users--;

						throw new GraphicalException("GLFW could not be initialized!");
					}
				}, true);
			} catch (final Exception e) {
				throw new GraphicalException(e);
			}

			glfwErrorCallback = GLFWErrorCallback.create(GraphicalSystem::onError);
			glfwSetErrorCallback(glfwErrorCallback);

			(pollThread = new Thread(GraphicalSystem::pollLoop)).start();
		}
	}

	public static synchronized final void release() {
		if (users == 0) {
			throw new IllegalStateException("Tried to release GLFW but it is not in use!");
		}

		if (--users == 0) {
			pollThread.interrupt();

			boolean interrupted = false;
			while (true) {
				try {
					pollThread.join();
					break;
				} catch (final InterruptedException ie) {
					interrupted = true;
				}
			}

			MainThreadUtils.run(() -> {
				glfwErrorCallback.free();
				glfwTerminate();
			}, true);

			if (interrupted) {
				Thread.currentThread().interrupt();
			}
		}
	}

	private static final void pollLoop() {
		final Thread thread = Thread.currentThread();

		while (!thread.isInterrupted()) {
			MainThreadUtils.run(GLFW::glfwWaitEvents, true);
		}
	}

	public static void ensure() {
		if (users <= 0) {
			throw new IllegalArgumentException("GLFW must be active!");
		}
	}

	private static void onError(final int error, final long description) {
		errorEvent.fire(new Pair<>(error, description));
	}

	static {
		errorEvent.addListener((data) -> {
			Log.log("[GraphicalSystem] GLFW error %d, desc %d", data.left, data.right);
		});
	}
}
