package me.danielzgtg.compsci11_sem2_2017.newgraphics;

public final class CameraShaderData {

	public CameraShaderData(final Shader shader) {
		this.projMatrixLocation = shader.getUniform("projM");
		this.viewMatrixLocation = shader.getUniform("viewM");
		this.modelMatrixLocation = shader.getUniform("modelM");
	}

	public final int projMatrixLocation;
	public final int viewMatrixLocation;
	public final int modelMatrixLocation;
}
