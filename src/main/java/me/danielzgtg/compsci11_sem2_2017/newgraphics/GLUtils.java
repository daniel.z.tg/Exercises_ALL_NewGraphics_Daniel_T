package me.danielzgtg.compsci11_sem2_2017.newgraphics;

import java.util.function.Function;

import java.io.InputStream;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;

import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL11.GL_NO_ERROR;
import static org.lwjgl.opengl.GL11.glGetError;
import static org.lwjgl.opengl.GL20.GL_LINK_STATUS;
import static org.lwjgl.opengl.GL20.GL_VALIDATE_STATUS;
import static org.lwjgl.opengl.GL20.glDeleteProgram;
import static org.lwjgl.opengl.GL20.glDeleteShader;
import static org.lwjgl.opengl.GL20.glDetachShader;
import static org.lwjgl.opengl.GL20.glGetProgrami;

@SuppressWarnings("WeakerAccess")
public final class GLUtils {

	public static final Function<String, InputStream> NEWGRAPHICS_RESOURCE_LOADER =
			GLUtils.class.getClassLoader()::getResourceAsStream;

	public static final void assertNoError() {
		int error = glGetError();
		if (error != GL_NO_ERROR) {
			throw new GraphicalException(String.valueOf(error));
		}
	}

	public static final void assertNoShaderError(final int shaderID) {
		int error = glGetError();
		if (error != GL_NO_ERROR) {
			glDeleteShader(shaderID);
			throw new GraphicalException(String.valueOf(error));
		}
	}

	public static final void assertNoLinkError(final int programID, final int vshID, final int fshID) {
		if (glGetProgrami(programID, GL_LINK_STATUS) == GL_FALSE ||
				glGetProgrami(programID, GL_VALIDATE_STATUS) == GL_FALSE) {
			glDetachShader(programID, vshID);
			glDetachShader(programID, fshID);
			glDeleteShader(vshID);
			glDeleteShader(fshID);
			glDeleteProgram(programID);

			throw new GraphicalException("Bad shader program");
		}
	}

	public static final FloatBuffer createMatrixBuffer() {
		return BufferUtils.createFloatBuffer(16);
	}

	@Deprecated
	private GLUtils() { throw new UnsupportedOperationException(); }
}
