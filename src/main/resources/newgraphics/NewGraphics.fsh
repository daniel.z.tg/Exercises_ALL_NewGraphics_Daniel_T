#version 330 core

uniform sampler2D texture_diffuse;

in vec4 vColor;
in vec2 vTexCoord;

out vec4 fragColor;

void main()
{
    fragColor = texture(texture_diffuse, vTexCoord) * vColor;
}
