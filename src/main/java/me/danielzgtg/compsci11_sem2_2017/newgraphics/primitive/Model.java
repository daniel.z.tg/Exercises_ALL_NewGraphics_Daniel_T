package me.danielzgtg.compsci11_sem2_2017.newgraphics.primitive;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import me.danielzgtg.compsci11_sem2_2017.common.reflect.IBuilder;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.GLUtils;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.GraphicalException;
import me.danielzgtg.compsci11_sem2_2017.newgraphics.RelativeSubview;
import org.lwjgl.BufferUtils;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_SHORT;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glBufferSubData;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

@SuppressWarnings("WeakerAccess")
public final class Model {
	private final Primitive[] primitives;
	private final List<Primitive> primitiveList;
	private final FloatBuffer vertices;
	private final ShortBuffer indices;
	private final int[] verticesSubarrayPositions;
	private final int[] indicesSubarrayPositions;
	private final int triangles;
	private int vaoID;
	private int vboID;
	private int eboID;

	public final RelativeSubview modelView = new RelativeSubview();

	private static final int VERTEXATTRIB_FLOAT_SIZE = Float.SIZE / Byte.SIZE;

	private static final int VERTEXATTRIB_POSITION_ELEMENTS = 3;
	private static final int VERTEXATTRIB_COLOR_ELEMENTS = 4;
	private static final int VERTEXATTRIB_TEXTURE_ELEMENTS = 2;
	private static final int VERTEXATTRIB_POSITION_SIZE = VERTEXATTRIB_POSITION_ELEMENTS * VERTEXATTRIB_FLOAT_SIZE;
	private static final int VERTEXATTRIB_COLOR_SIZE  = VERTEXATTRIB_COLOR_ELEMENTS * VERTEXATTRIB_FLOAT_SIZE;
	private static final int VERTEXATTRIB_TEXTURE_SIZE = VERTEXATTRIB_TEXTURE_ELEMENTS * VERTEXATTRIB_FLOAT_SIZE;

	private static final int VERTEXATTRIB_STRIDE =
			VERTEXATTRIB_POSITION_SIZE + VERTEXATTRIB_COLOR_SIZE + VERTEXATTRIB_TEXTURE_SIZE;

	private static final long VERTEXATTRIB_POSITION_OFFSET = 0;
	private static final long VERTEXATTRIB_COLOR_OFFSET    = VERTEXATTRIB_POSITION_OFFSET + VERTEXATTRIB_POSITION_SIZE;
	private static final long VERTEXATTRIB_TEXTURE_OFFSET  = VERTEXATTRIB_COLOR_OFFSET + VERTEXATTRIB_COLOR_SIZE;

	private Model(final List<Function<Model, Primitive>> requestedPrimitives) {
		final List<Primitive> tmpPrimitives = new LinkedList<>();

		for (final Function<Model, Primitive> request : requestedPrimitives) {
			tmpPrimitives.add(request.apply(this));
		}

		final int length = tmpPrimitives.size();
		this.primitives = new Primitive[length];
		this.verticesSubarrayPositions = new int[length];
		this.indicesSubarrayPositions = new int[length];

		int i = 0;
		int verticesCount = 0;
		int indicesCount = 0;
		int triangles = 0;
		for (final Primitive primitive : tmpPrimitives) {
			this.verticesSubarrayPositions[i] = verticesCount;
			this.indicesSubarrayPositions[i] = indicesCount;

			verticesCount += primitive.vertices.length;
			indicesCount += primitive.indices.length;

			this.primitives[i++] = primitive;
			triangles += primitive.getTriangles();
		}

		this.primitiveList = Collections.unmodifiableList(tmpPrimitives);

		this.vertices = BufferUtils.createFloatBuffer(verticesCount);
		this.indices = BufferUtils.createShortBuffer(indicesCount);
		this.triangles = triangles;
	}

	public RelativeSubview getModelView() {
		return this.modelView;
	}

	public float getPosX() {
		return this.modelView.getPosX();
	}

	public float getPosY() {
		return this.modelView.getPosY();
	}

	public float getPosZ() {
		return this.modelView.getPosZ();
	}

	public float getYaw() {
		return this.modelView.getYaw();
	}

	public float getPitch() {
		return this.modelView.getPitch();
	}

	public void setPos(final float posX, final float posY, final float posZ) {
		this.modelView.setPos(posX, posY, posZ);
	}

	public void setPosX(final float posX) {
		this.modelView.setPosX(posX);
	}

	public void setPosY(final float posY) {
		this.modelView.setPosY(posY);
	}

	public void setPosZ(final float posZ) {
		this.modelView.setPosZ(posZ);
	}

	public void setRot(final float yaw, final float pitch) {
		this.modelView.setRot(yaw, pitch);
	}

	public void setYaw(final float yaw) {
		this.modelView.setYaw(yaw);
	}

	public void setPitch(final float pitch) {
		this.modelView.setPitch(pitch);
	}

	public final List<Primitive> getPrimitives() {
		return this.primitiveList;
	}

	public final void reupload() {
		this.cleanup();

		glBindVertexArray(this.vaoID = glGenVertexArrays());

		glBindBuffer(GL_ARRAY_BUFFER, this.vboID = glGenBuffers());
		glBufferData(GL_ARRAY_BUFFER, this.vertices, GL_DYNAMIC_DRAW);

		glVertexAttribPointer(0, VERTEXATTRIB_POSITION_ELEMENTS, GL_FLOAT, false,
				VERTEXATTRIB_STRIDE, VERTEXATTRIB_POSITION_OFFSET);
		glVertexAttribPointer(3, VERTEXATTRIB_COLOR_ELEMENTS, GL_FLOAT, false,
				VERTEXATTRIB_STRIDE, VERTEXATTRIB_COLOR_OFFSET);
		glVertexAttribPointer(8, VERTEXATTRIB_TEXTURE_ELEMENTS, GL_FLOAT, false,
				VERTEXATTRIB_STRIDE, VERTEXATTRIB_TEXTURE_OFFSET);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this.eboID = glGenBuffers());
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, this.indices, GL_DYNAMIC_DRAW);

		try {
			GLUtils.assertNoError();
		} catch (final Exception e) {
			this.cleanup();
			throw e;
		}
	}

	public final void flipBuffers() {
		final int length = this.primitives.length;

		boolean verticesDirty = false;
		boolean indicesDirty = false;
		for (int i = 0; i < length; i++) {
			final Primitive primitive = this.primitives[i];

			if (primitive.dirty) {
				if (primitive.verticesDirty) {
					final float[] vertices = primitive.vertices;
					this.vertices.put(vertices, this.verticesSubarrayPositions[i], vertices.length);

					primitive.verticesDirty = false;
					verticesDirty = true;
				}

				if (primitive.indicesDirty) {
					final short[] indices = primitive.indices;
					this.indices.put(indices, this.indicesSubarrayPositions[i], indices.length);

					primitive.indicesDirty = false;
					indicesDirty = true;
				}

				primitive.dirty = false;
			}
		}

		this.bind();

		if (verticesDirty) {
			this.vertices.flip();

			glBindBuffer(GL_ARRAY_BUFFER, this.vboID);
			glBufferSubData(GL_ARRAY_BUFFER, 0, this.vertices);
		}

		if (indicesDirty) {
			this.indices.flip();

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this.eboID);
			glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, this.indices);
		}

		try {
			GLUtils.assertNoError();
		} catch (final Exception e) {
			this.cleanup();
			throw e;
		}
	}

	public final void cleanup() {
		if (this.vaoID != 0) {
			glBindVertexArray(this.vaoID);

			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glDeleteBuffers(this.vboID);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			glDeleteBuffers(this.eboID);

			glBindVertexArray(0);
			glDeleteVertexArrays(this.vaoID);

			this.vaoID = 0;
			this.vboID = 0;
			this.eboID = 0;
		}
	}

	public final void draw() {
		this.bind();

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(3);
		glEnableVertexAttribArray(8);

		glDrawElements(GL_TRIANGLES, this.triangles * 3, GL_UNSIGNED_SHORT, 0);

		unbind();
	}

	public final void bind() {
		if (this.vaoID == 0) {
			throw new GraphicalException();
		}

		glBindVertexArray(this.vaoID);
	}

	public static final void unbind() {
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	@Override
	public final void finalize() {
		if (this.vaoID != 0) {
			System.err.println("[Model] GPU MODEL RESOURCE LEAK!" + this.vaoID);
		}
	}

	public static final Builder builder() {
		return new Builder();
	}

	public static final class Builder implements IBuilder<Model> {
		private /*mut*/ final List<Function<Model, Primitive>> requestedPrimitives = new LinkedList<>();

		public final Model.Builder triangle() {
			this.requestedPrimitives.add(Triangle::new);

			return this;
		}

		public final Model.Builder quad() {
			this.requestedPrimitives.add(Quad::new);

			return this;
		}

		@Override
		public Model build() {
			return new Model(this.requestedPrimitives);
		}
	}
}
