package me.danielzgtg.compsci11_sem2_2017.newgraphics.primitive;

public class Quad extends Primitive {

	/*packaged*/ Quad(final Model parent) {
		super(new float[] {
				//  x,     y,     z,   r, g, b, a,  u, v
				+0.0f, +0.0f, +0.0f,   0, 0, 0, 0,  0, 0,
				+0.0f, +0.0f, +0.0f,   0, 0, 0, 0,  0, 0,
				+0.0f, +0.0f, +0.0f,   0, 0, 0, 0,  0, 0,
				+0.0f, +0.0f, +0.0f,   0, 0, 0, 0,  0, 0
		}, new short[] {
				0, 1, 2,
				2, 3, 0
		}, parent);
	}

	@Override
	public void show() {
		this.indices[2] = 0;
		this.indices[3] = 0;

		this.dirty = true;
		this.indicesDirty = true;
	}

	@Override
	public void hide() {
		this.indices[2] = 2;
		this.indices[3] = 2;

		this.dirty = true;
		this.indicesDirty = true;
	}
}
