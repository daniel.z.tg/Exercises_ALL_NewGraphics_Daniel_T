package me.danielzgtg.compsci11_sem2_2017.newgraphics.primitive;

public final class Triangle extends Primitive {

	/*packaged*/ Triangle(final Model parent) {
		super (new float[] {
				//  x,     y,     z,   r, g, b, a,  u, v
				+0.0f, +0.0f, +0.0f,   0, 0, 0, 0,  0, 0,
				+0.0f, +0.0f, +0.0f,   0, 0, 0, 0,  0, 0,
				+0.0f, +0.0f, +0.0f,   0, 0, 0, 0,  0, 0
		}, new short[] {
				0, 1, 2
		}, parent);
	}

	@Override
	public void show() {
		this.indices[1] = 0;

		this.dirty = true;
		this.indicesDirty = true;
	}

	@Override
	public void hide() {
		this.indices[1] = 1;

		this.dirty = true;
		this.indicesDirty = true;
	}
}
