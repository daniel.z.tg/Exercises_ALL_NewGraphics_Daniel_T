package me.danielzgtg.compsci11_sem2_2017.newgraphics;

import java.nio.ByteBuffer;

import org.lwjgl.BufferUtils;

import static org.lwjgl.opengl.GL11.GL_LINEAR;
import static org.lwjgl.opengl.GL11.GL_NEAREST;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_S;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL11.GL_UNPACK_ALIGNMENT;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glDeleteTextures;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glPixelStorei;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;
import static org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL30.glGenerateMipmap;

@SuppressWarnings("WeakerAccess")
public final class Texture {

	private volatile int texID = 0;
	private final ByteBuffer texBuffer;
	private final int width;
	private final int height;

	private Texture(final int width, final int height, final ByteBuffer texBuffer) {
		this.width = width;
		this.height = height;
		this.texBuffer = texBuffer;
	}

	public final void bind() {
		if (this.texID == 0) {
			throw new GraphicalException();
		}

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, this.texID);
	}

	public static final void unbind() {
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	public final void cleanup() {
		if (this.texID != 0) {
			unbind();
			glDeleteTextures(this.texID);
			this.texID = 0;
		}
	}

	@Override
	public void finalize() {
		if (this.texID != 0) {
			System.err.println("[Texture] GPU TEXTURE RESOURCE LEAK!" + this.texID);
		}
	}

	public final void reupload() {
		GLUtils.assertNoError();
		this.cleanup();

		final int texID = glGenTextures();
		glBindTexture(GL_TEXTURE_2D, texID);
		glActiveTexture(GL_TEXTURE0);
		GLUtils.assertNoError();

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		GLUtils.assertNoError();

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		GLUtils.assertNoError();
		glTexImage2D(
				GL_TEXTURE_2D, 0, GL_RGBA, this.width, this.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, this.texBuffer);
		glGenerateMipmap(GL_TEXTURE_2D);
		GLUtils.assertNoError();

		GLUtils.assertNoError();
		this.texID = texID;
	}

	public static final Texture load(final int width, final int height, final byte[] texture) {
		if (width == 0 || height == 0 || texture == null) {
			throw new IllegalArgumentException();
		}

		final int length = width * height * 4;
		if (texture.length != length) {
			throw new IllegalArgumentException();
		}

		final ByteBuffer texBuffer = BufferUtils.createByteBuffer(length);
		texBuffer.put(texture).flip();

		return new Texture(width, height, texBuffer);
	}

	public static final Texture loadWhite() {
		return load(1, 1, new byte[] {
				(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xF
		});
	}
}
