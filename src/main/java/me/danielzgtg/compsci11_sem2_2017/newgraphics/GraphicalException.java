package me.danielzgtg.compsci11_sem2_2017.newgraphics;

@SuppressWarnings({"WeakerAccess", "unused"})
public class GraphicalException extends IllegalStateException {

	public GraphicalException() {
		super();
	}

	public GraphicalException(final String s) {
		super(s);
	}

	public GraphicalException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public GraphicalException(final Throwable cause) {
		super(cause);
	}
}
